//Human_motionTracking + observed trajectory + graphical result of trajectory and moving position.

//Developed by  Vibeananda Dutta.

#include <iostream>
#include <opencv2/opencv.hpp>
#include <iomanip>
#include <highgui.h>
#include <string.h>
#include <cmath>
#define PI 3.14159265
using namespace std;
using namespace cv;

//My sensitivity value to be used in the absdiff() function
const static int SENSITIVITY_VALUE = 30;
//size of blur used to smooth the intensity image output from absdiff() function
const static int BLUR_SIZE = 20;
//we'll have just one object to search for
//and keep track of its position.
int theObject[2] = {-1,-1};
//bounding rectangle of the object, we will use the center of this as its position.
Rect objectBoundingRectangle; //= Rect(0,0,0,0);
//Rect rectangle;

//these two can be toggled by pressing 'd' or 't'
double Z;
double initialZ;
double dX,dY,dZ;
int i=0;
float angl;
int xpos,x;
int ypos,y;
//int to string helper function
string intToString(int number){

	//this function has a number input and string output
	std::stringstream ss;
	ss << number;
	return ss.str();
}

void initialValue(int z)
{
    if(i==0)
    {
        initialZ = z;
        i++;
    }
}


void searchForMovement(Mat thresholdImage, Mat &cameraFeed){
	//notice how we use the '&' operator for objectDetected and cameraFeed. This is because we wish
	//to take the values passed into the function and manipulate them, rather than just working with a copy.
	//eg. we draw to the cameraFeed to be displayed in the main() function.
	bool objectDetected = false;
        //bool motionDetected = false;
        Moments mu;
	Mat temp;
	thresholdImage.copyTo(temp);
        mu = moments(temp,true);
        double area = mu.m00;
        //Create a black image with the size as the camera output
          //Mat imgLines = Mat::zeros( cameraFeed.size(), CV_8UC3 );;
	//these two vectors needed for output of findContours
	vector< vector<Point> > contours;{
        FileStorage storage("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/contours.yml", FileStorage::WRITE);
                                        //Mat rec_img
                         storage<<"Postion value"<<contours;
                         storage.release();}
	vector<Vec4i> hierarchy;
        //vector<Rect> boundRect( contours.size() );
	//find contours of filtered image using openCV findContours function
	findContours(temp,contours,hierarchy,CV_RETR_CCOMP,CV_CHAIN_APPROX_SIMPLE );// retrieves all contours
	findContours(temp,contours,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE );// retrieves external contours
        // Create a black image with the size as the camera output                
             Mat imgLines = Mat::zeros(cameraFeed.size(),CV_8UC3);
	//if contours vector is not empty, we have found some objects
	if(contours.size()>0)
              objectDetected=true;
  
             //motionDetected=true;}
	else 
           objectDetected = false;
           //motionDetected = false;}
  
	if(objectDetected){
		//the largest contour is found at the end of the contours vector
		//we will simply assume that the biggest contour is the object we are looking for.
		vector< vector<Point> > largestContourVec;
                //vector<Rect> boundRect( contours.size() );
		largestContourVec.push_back(contours.at(contours.size()-1));
		//make a bounding rectangle around the largest contour then find its centroid
		//this will be the object's final estimated position.
	        objectBoundingRectangle = boundingRect(largestContourVec.at(0));
           	xpos = objectBoundingRectangle.x+objectBoundingRectangle.width/2;
		ypos = objectBoundingRectangle.y+objectBoundingRectangle.height/2;
                
                //objectBoundingRectangle(cameraFeed,Point(objectBoundingRectangle.x,objectBoundingRectangle.y),20, Scalar(0,255,0),2);
                 FileStorage storage("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/larg_con_vec.yml", FileStorage::WRITE);
                                        //Mat rec_img
                         storage<<"Postion value"<<largestContourVec;
                         storage.release();
                         Z = area;
                 //Draw a red line from the previous point to the current point
                if(theObject[0] >0 && theObject[1]>0 && xpos>0 && ypos>0){
                   dX = objectBoundingRectangle.x -325;
                   dY = 233-objectBoundingRectangle.x ;
                   dZ = Z - initialZ;
               cout<<" dX = "<<dX<<" dY = "<<dY<<" dZ = "<<dZ<<endl;
           //Write the position of human in each frame and its motion in a yml file
         line(imgLines, Point(xpos,ypos), Point(theObject[0],theObject[1]), Scalar(0,0,255),2);}
		//update the objects positions by changing the 'theObject' array values
		theObject[0] = xpos , theObject[1] = ypos;
	}
	//make some temp x and y variables so we dont have to type out so much
	x = theObject[0];
	y = theObject[1];
      // Findind the postion of motion @ each frame
        cout<<" X-Position:\n"<<x<<endl;
        cout<<" Y-Position:\n"<<y<<endl;

        cameraFeed = cameraFeed + imgLines;
	 //imshow("Final Threshold Image",temp);
         //Rect(cameraFeed,Point(objectBoundingRectangle.x,objectBoundingRectangle.y),20,Scalar(0,255,0),2);
	//draw some crosshairs around the object
        rectangle(cameraFeed,Point(objectBoundingRectangle.x,objectBoundingRectangle.y),Point(x,y),Scalar(0,255,0),2,8,0);
       circle(cameraFeed,Point(x,y),20,Scalar(0,0,0),2);
	/*line(cameraFeed,Point(x,y),Point(x,y-25),Scalar(0,255,0),2);
	line(cameraFeed,Point(x,y),Point(x,y+25),Scalar(0,255,0),2);
	line(cameraFeed,Point(x,y),Point(x-25,y),Scalar(0,255,0),2);
	line(cameraFeed,Point(x,y),Point(x+25,y),Scalar(0,255,0),2);*/
           
            //Write the position of human in each frame and its motion in a yml file
         line(cameraFeed, Point(x,y), Point(x+1,y+1), Scalar(255,0,0),2);
         FileStorage storage("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/rec_posi.yml", FileStorage::WRITE);
                                        //Mat rec_img
                         storage<<"Postion value"<< x;
                         storage.release(); 

	//write the position of the object to the screen
	putText(cameraFeed,"Tracking Human-Motion with(COG) at postion (" + intToString(x)+","+intToString(y)+")",Point(x,y),1,1,Scalar(255,0,0),2);
  // return motionDetected;
        
}

//find the angle of the motion

void Angle(Mat &cameraFeed)
{
   // char y[50];

    if(dX>0 && dY>0)
    {
        angl = atan(dY/dX) * (180/PI);
    }

    if(dX<0 && dY>0 )
    {
        angl = (PI+(atan(dY/dX)))*(180/PI);
    }

    if(dX>0 && dY<0)
    {
        angl = (2*PI+(atan(dY/dX)))*(180/PI);
    }


    //sprintf(y,"Angle = %f",angl);
     cout <<"Angle =  " <<angl <<endl;
    //putText(img,y,Point(355,203),CV_FONT_HERSHEY_COMPLEX_SMALL,1,Scalar(255,0,0),1,8);

}

int main(){

	//some boolean variables for added functionality
	bool objectDetected = false;
	//these two can be toggled by pressing 'd' or 't'
	bool debugMode = false;
	bool trackingEnabled = false;
	//pause and resume code
	bool pause = false;
        // record and save the video fame of the current frame to used in future  prediction method
         bool recording = false;
	bool startNewRecording = false;
         //bool motionDetected = false;
	int inc=0;
	bool firstRun = true;
        int fps =3;
        int numBins = 256;
        float range[] ={0,255};
        const float *ranges = { range };
        bool uniform = true; bool accumulate = false;
        VideoWriter oVideoWriter;//create videoWriter object, not initialized yet
        	//set up the matrices that we will need
	//the two frames we will be comparing
	Mat frame1,frame2;

	//their grayscale images (needed for absdiff() function)
	Mat grayImage1,grayImage2;

	//resulting difference image
	Mat differenceImage;

	//thresholded difference image (for use in findContours() function)
	Mat thresholdImage;
        
         // B G R Image Matrix
        Mat b_hist, g_hist, r_hist;  
        Mat hist;
	//video capture object.
	VideoCapture capture;
             
	//VideoWriter writer;

	while(1){
                      capture.open(0);
        double dWidth = capture.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
	double dHeight = capture.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

	cout << "Frame Size = " << dWidth << "x" << dHeight << endl;

          //set framesize for use with videoWriter
	       Size frameSize(static_cast<int>(dWidth), static_cast<int>(dHeight));

		if(!capture.isOpened()){
			cout<<"ERROR ACQUIRING VIDEO FEED\n";
			//getchar();
			return -1;
                       }
		//we can loop the video by re-opening the capture every time the video reaches its last frame

		
		//check if the video has reach its last frame.
		//we add '-1' because we are reading two frames from the video at a time.
		//if this is not included, we get a memory error!
		//while(capture.get(CV_CAP_PROP_POS_FRAMES)<capture.get(CV_CAP_PROP_FRAME_COUNT)-1){

			//read first frame
			capture.read(frame1);
			//convert frame1 to gray scale for frame differencing
			cv::cvtColor(frame1,grayImage1,COLOR_BGR2GRAY);
			//copy second frame
			capture.read(frame2);
			//convert frame2 to gray scale for frame differencing
			cv::cvtColor(frame2,grayImage2,COLOR_BGR2GRAY);
			//perform frame differencing with the sequential images. This will output an "intensity image"
			//do not confuse this with a threshold image, we will need to perform thresholding afterwards.
			cv::absdiff(grayImage1,grayImage2,differenceImage);
			//threshold intensity image at a given sensitivity value
			cv::threshold(differenceImage,thresholdImage,SENSITIVITY_VALUE,255,THRESH_BINARY);
			//if(debugMode==true){
				//show the difference image and threshold image
				cv::imshow("Difference Image",differenceImage);
				cv::imshow("Threshold Image", thresholdImage);
			//}else{
				//if not in debug mode, destroy the windows so we don't see them anymore
				//cv::destroyWindow("Difference Image");
				//cv::destroyWindow("Threshold Image");
			//}
			//blur the image to get rid of the noise. This will output an intensity image
			cv::blur(thresholdImage,thresholdImage,cv::Size(BLUR_SIZE,BLUR_SIZE));
			//threshold again to obtain binary image from blur output
			cv::threshold(thresholdImage,thresholdImage,SENSITIVITY_VALUE,255,THRESH_BINARY);
			//if(debugMode==true){
				//show the threshold image after it's been "blurred"

				imshow("Final Threshold Image",thresholdImage);
                             
                              
                       Mat output(thresholdImage.size(),thresholdImage.type());
                 FileStorage storage("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/rec_imgval.yml", FileStorage::WRITE);
                                        //Mat rec_img
                         storage<<"image value"<< grayImage2;
                         storage.release(); 

                     // Separate the image in 3 places (B, G and R)
                                vector<Mat> bgr_planes;
                               split( frame1, bgr_planes);    
                          
			//}
			//else {
				//if not in debug mode, destroy the windows so we don't see them anymore
				//cv::destroyWindow("Final Threshold Image");
			//}

			//if tracking enabled, search for contours in our thresholded image
			if(trackingEnabled){

				searchForMovement(thresholdImage,frame1);
                              
                                //motionDetected(thresholdImage,frame1);
                              putText(frame1,"MOTION DETECTED",cv::Point(0,420),2,2,cv::Scalar(0,255,0));
                                // call the function angle here to display the result
                                Angle(frame1);
                //draw time stamp to video in bottom left corner. We draw it before we write so that it is written on the video file.
		               

			}
                     //  else{ 
			//reset our variables if tracking is disabled
			//recording = false;
			//motionDetected = false;
		//}
    
		//putText(frame1,getDateTime(),Point(0,480),1,1,Scalar(0,0,0),2);
                          
			//show our captured frame
			imshow("Frame1",frame1);
 
       //compute the histogram
                          calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &numBins, &ranges , uniform, accumulate);
                           calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &numBins, &ranges , uniform, accumulate);
                            calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &numBins, &ranges , uniform, accumulate);
                             calcHist( &differenceImage, 1, 0, Mat(), hist, 1, &numBins, &ranges , uniform, accumulate);
                // Draw the histogram for the resulting Image
                           int hist_w = 512; int hist_h = 400;
                           int bin_w = cvRound( (double) hist_w/numBins); 
                           Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(255,255,255));
                           Mat histImage1(hist_h, hist_w, CV_8UC1, Scalar(255,255,255)); 
                  // Normalized the result 
                    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
                    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
                    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
                    normalize(hist, hist, 0, histImage1.rows, NORM_MINMAX, -1, Mat()); 
                    //Draw for Each Channel (B G R)
                      for (int i=1; i<numBins; i++){
                        line(histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ),
                              Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ), Scalar( 255,0,0),2,8,0);
                        line(histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ),
                              Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ), Scalar( 0,255,0),2,8,0);
                        line(histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ),
                              Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ), Scalar( 0,0,255),2,8,0);
                        line(histImage1, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ),
                              Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ), Scalar(0,0,0),2,8,0);
                        }
                           //display the histogram

                              namedWindow("calculate Histogram", CV_WINDOW_AUTOSIZE);
                              imshow("calculate Histogram",histImage);  

                          //Display the histogram of contour displacement
                                 namedWindow("calculate Histogram of contour displacement", CV_WINDOW_AUTOSIZE);
                              imshow("calculate Histogram of contour displacement",histImage1); 

	//VideoCapture cap(0); // open the video camera no. 0
	
	namedWindow("MyVideo",CV_WINDOW_AUTOSIZE); //create a window called "MyVideo"

	
	while (1) {

		if(startNewRecording==true){
			
			oVideoWriter  = VideoWriter("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/MyVideo0.avi", CV_FOURCC('D', 'I', 'V', '3'), fps, frameSize,true);

			recording = true;
			startNewRecording = false;
			cout<<"New video file created /home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/MyVideo" +intToString(inc)+".avi "<<endl;

			if ( !oVideoWriter.isOpened() ) //if not initialize the VideoWriter successfully, exit the program
			{
				cout << "ERROR: Failed to initialize video writing" << endl;
				//getchar();
				return -1;
			}

		}


		Mat frame;

		bool bSuccess = capture.read(frame); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			cout << "ERROR: Cannot read a frame from video file" << endl;
			break;
		}

		//if we're in recording mode, write to file
		if(recording){

			oVideoWriter.write(frame);
			//show "REC" in top left corner in red
			//be sure to do this AFTER you write to the file so that "REC" doesn't show up
			//on the recorded video.
			putText(frame,"REC",Point(0,60),2,2,Scalar(0,0,255),2);


		}
		
                     imshow("MyVideo", frame); //show the frame in "MyVideo" window

		    
			//check to see if a button has been pressed.
			//this 10ms delay is necessary for proper operation of this program
			//if removed, frames will not have enough time to referesh and a blank 
			//image will appear.
			switch(waitKey(10)){
                        
                       case 114:
			//'r' has been pressed.
			//toggle recording mode
			recording =!recording;

			if(firstRun == true){

				cout << "New Recording Started" << endl;
				oVideoWriter  = VideoWriter("/home/vibek/Desktop/Research Work/Human Detection with Seekur Jr/Robust Method/MyVideo0.avi", CV_FOURCC('D', 'I', 'V', '3'), fps, frameSize,true);

				if ( !oVideoWriter.isOpened() ) 
			{
				cout << "ERROR: Failed to initialize video writing" << endl;
				//getchar();
				return -1;
			}
				firstRun = false;


			}
			else {if (!recording)cout << "Recording Stopped" << endl;

			else cout << "Recording Started" << endl;
			}
			break;

		case 110:
			//'n' has been pressed
			//start new video file
			startNewRecording = true;
			cout << "New Recording Started" << endl;
			//increment video file name
			inc+=1;
			break; 
			     //break; 

			case 27: //'esc' key has been pressed, exit program.
				return 0;
			case 116: //'t' has been pressed. this will toggle tracking
				trackingEnabled = !trackingEnabled;
				if(trackingEnabled == false) 
                                  cout<<"Tracking disabled."<<endl;
				else 
                                    cout<<"Tracking enabled."<<endl;
				break;
			case 100: //'d' has been pressed. this will debug mode
				debugMode = !debugMode;
				if(debugMode == false) 
                                         cout<<"Debug mode disabled."<<endl;
				else 
                                       cout<<"Debug mode enabled."<<endl;
				break;
			case 112: //'p' has been pressed. this will pause/resume the code.
				pause = !pause;
				if(pause == true){ 
                                            cout<<"Code paused, press 'p' again to resume"<<endl;
				while (pause == true){
					//stay in this loop until 
					switch (waitKey()){
						//a switch statement inside a switch statement? Mind blown.
					case 112: 
						//change pause back to false
						pause = false;
						cout<<"Code Resumed"<<endl;
						break;
					}
				}
				}



			}
		//}
		//release the capture before re-opening and looping again.
		capture.release();
	}
}
	return 0;

}
